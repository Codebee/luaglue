#include <iostream>
#include <luaglue/luaglue.hpp> // include the luaglue header file

static void helloworld(void)
{
    std::cout << "hello world" << std::endl;
}

extern "C" int luaopen_helloworld(lua_State *L)
{
    luaglue::scope(L) // export the symbols to _G
    [
        luaglue::def("helloworld", helloworld) // export the function helloworld name as "helloworld"
    ];

    return 0;
}


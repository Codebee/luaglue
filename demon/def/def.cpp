#include <iostream>
#include <luaglue/luaglue.hpp>

static void helloworld(void)
{
    std::cout << "hello world" << std::endl;
}

extern "C" int luaopen_def(lua_State *L)
{
    luaglue::def(L, "helloworld", helloworld);

    return 0;
}

